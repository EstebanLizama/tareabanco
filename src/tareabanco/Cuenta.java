/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareabanco;

import java.util.ArrayList;

/**
 *
 * @author Esteban
 */
public class Cuenta {
 private int Numcliente;  
 private int   Numcuenta;
 private String Tipodecuenta;   
 private double Saldo;
 private Tarjeta tarjetaCuenta;
 private ArrayList<Transaccion>ArrayTransaccion; 
 private Cliente c1;
private Banco b1;
    public Cuenta(int Numcliente, int Numcuenta, String Tipodecuenta, double Saldo,Tarjeta t1,Cliente c1,Banco b1) {
        this.Numcliente = Numcliente;
        this.Numcuenta = Numcuenta;
        this.Tipodecuenta = Tipodecuenta;
        this.Saldo = Saldo;
        this.tarjetaCuenta=t1;
   this.c1=c1;
   this.b1=b1;
    }
 
 
 public void consultarSaldo(){}

    public int getNumcliente() {
        return Numcliente;
    }

    public void setNumcliente(int Numcliente) {
        this.Numcliente = Numcliente;
    }

    public int getNumcuenta() {
        return Numcuenta;
    }

    public void setNumcuenta(int Numcuenta) {
        this.Numcuenta = Numcuenta;
    }

    public String getTipodecuenta() {
        return Tipodecuenta;
    }

    public void setTipodecuenta(String Tipodecuenta) {
        this.Tipodecuenta = Tipodecuenta;
    }

    public double getSaldo() {
        return Saldo;
    }

    public void setSaldo(double Saldo) {
        this.Saldo = Saldo;
    }

    public Tarjeta getTarjetaCuenta() {
        return tarjetaCuenta;
    }

    public void setTarjetaCuenta(Tarjeta tarjetaCuenta) {
        this.tarjetaCuenta = tarjetaCuenta;
    }

    public ArrayList<Transaccion> getArrayTransaccion() {
        return ArrayTransaccion;
    }

    public void setArrayTransaccion(ArrayList<Transaccion> ArrayTransaccion) {
        this.ArrayTransaccion = ArrayTransaccion;
    }

    public Cliente getC1() {
        return c1;
    }

    public void setC1(Cliente c1) {
        this.c1 = c1;
    }

    public Banco getB1() {
        return b1;
    }

    public void setB1(Banco b1) {
        this.b1 = b1;
    }

    @Override
    public String toString() {
        return "Cuenta{" + "Numcliente=" + Numcliente + ", Numcuenta=" + Numcuenta + ", Tipodecuenta=" + Tipodecuenta + ", Saldo=" + Saldo + ", tarjetaCuenta=" + tarjetaCuenta + ", ArrayTransaccion=" + ArrayTransaccion + ", c1=" + c1 + ", b1=" + b1 + '}';
    }


}
