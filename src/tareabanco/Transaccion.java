/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareabanco;

import java.util.Date;

/**
 *
 * @author Esteban
 */
public class Transaccion {
 private Date fecha ;   
 private String descripcion  ;   
 private String tipo ;   
 private double valor ;
 private Cuenta cuentaTrans;
private Cajero cajeroTrans;

    public Transaccion(Date fecha, String descripcion, String tipo, double valor, Cuenta cuentaTrans) {
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.valor = valor;
        this.cuentaTrans = cuentaTrans;
        
    }

public void registrar(){}

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Cuenta getCuentaTrans() {
        return cuentaTrans;
    }

    public void setCuentaTrans(Cuenta cuentaTrans) {
        this.cuentaTrans = cuentaTrans;
    }

    public Cajero getCajeroTrans() {
        return cajeroTrans;
    }

    public void setCajeroTrans(Cajero cajeroTrans) {
        this.cajeroTrans = cajeroTrans;
    }

    @Override
    public String toString() {
        return "Transaccion{" + "fecha=" + fecha + ", descripcion=" + descripcion + ", tipo=" + tipo + ", valor=" + valor + ", cuentaTrans=" + cuentaTrans + ", cajeroTrans=" + cajeroTrans + '}';
    }



}
